$("#formActivation").submit(function(event) {

    $.ajax({
        url: $(this).attr("action"),
        type: "POST",
        data: JSON.stringify({
            email : $("#email").val(),
            firstName : $("#firstName").val(),
            lastName : $("#lastName").val(),
            password : $("#userPassword").val()
        }),
        contentType : 'application/json',
        success: function(response) {
            document.location.href = "/";
            // alert(response.head.message); // show response from the php script.
        },
        error: function(response) {
            alert(response.responseJSON.head.message);
        }
    });

    event.preventDefault();

    return false; // avoid to execute the actual submit of the form.
});