package utils

import com.joypeg.scamandrill.client.MandrillAsyncClient
import com.joypeg.scamandrill.models._
import spray.json.{JsArray, JsObject, JsString}
import scala.language.postfixOps

/**
 * Created by nikica on 24.3.2015..
 */
object MandrillManager {

  def sendTestMail() = {

    val msgJson = JsObject(
      ("html", JsString("<p>HTML Content</p>")),
      ("text", JsString("Text Content")),
      ("subject", JsString("Test Subject")),
      ("from_email", JsString("mailer@packagedbits.com")),
      ("from_name", JsString("Packaged Bits")),
      ("to", JsArray(
        JsObject(
          ("email", JsString("nikica.maric@gmail.com")),
          ("type", JsString("to"))
        )
      )),
      ("bcc_address", JsString("mario.vidovic.sb@gmail.com")),
      ("tracking_domain", null),
      ("signing_domain", null),
      ("return_path_domain", null)
    )

    val testMessage = new MSendMsg(
      html = "<p>HTML Content Comes Here</p>",
      text = "Text Content",
      subject = "Test Subject",
      from_email = "mailer@packagedbits.com",
      from_name = "Packaged Bits",
      to = List[MTo](new MTo(email = "nikica.maric@gmail.com")),
      bcc_address = "",
      tracking_domain = "",
      signing_domain = "",
      return_path_domain = ""
    )

    MandrillAsyncClient.messagesSend(MSendMessage(message = testMessage))

    val messageSca = new MSendMsg(
      html = "",
      text = "",
      subject = "",
      from_email = "",
      from_name = "",
      to = List[MTo](new MTo(email = "nikica.maric@gmail.com", `type` = "to")),
      bcc_address = "",
      tracking_domain = "",
      signing_domain = "",
      return_path_domain = "",
      merge_vars = List[MMergeVars](new MMergeVars("nikica.maric@gmail.com", List[MVars](MVars("LINK", "http://localhost:9000")))),
      view_content_link = true
    )

    MandrillAsyncClient.messagesSendTemplate(
      MSendTemplateMessage(
        template_name = "activation",
        template_content = List[MVars](),
        message = messageSca
      )
    )


  }

  def sendMail(fromEmail: String, fromName: String, toEmail: String, subject: String, content: String) = {
    val message = new MSendMsg(
      html = s"<p>$content</p>",
      text = "",
      subject = subject,
      from_email = fromEmail,
      from_name = fromName,
      to = List[MTo](new MTo(email = toEmail, `type` = "to")),
      bcc_address = "",
      tracking_domain = "",
      signing_domain = "",
      return_path_domain = ""
    )

    MandrillAsyncClient.messagesSend(
      MSendMessage(
        message = message
      )
    )
  }

  def sendMailUsingTemplate(templateName: String, vars: Map[String, String], toEmail: String) = {

    val mVars = vars.map{ case(k, v) => MVars(k, v) } toList

    val message = new MSendMsg(
      html = "",
      text = "",
      subject = "",
      from_email = "",
      from_name = "",
      to = List[MTo](new MTo(email = toEmail, `type` = "to")),
      bcc_address = "",
      tracking_domain = "",
      signing_domain = "",
      return_path_domain = "",
      merge_vars = List[MMergeVars](new MMergeVars(toEmail, mVars)),
      track_opens = true,
      track_clicks = true
    )

    MandrillAsyncClient.messagesSendTemplate(
      MSendTemplateMessage(
        template_name = templateName,
        template_content = List[MVars](),
        message = message
      )
    )
  }

}
