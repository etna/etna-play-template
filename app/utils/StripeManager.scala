package utils

import java.util

import com.stripe.Stripe
import com.stripe.model.{Customer, Event, Subscription}

import scala.util.Try

/**
 * Created by nikica on 5.3.2015..
 */
object StripeManager {

  def retrieveEvent(eventId: String): Try[Event] = {
    Try {
      Stripe.apiKey = ApplicationConfig.stripeSecretKey
      val event = Event.retrieve(eventId)
      event
    }
  }

  def subscribeCustomer(plan: String, createdBy: String, token: String): Try[Customer] = {
    Try {
      Stripe.apiKey = ApplicationConfig.stripeSecretKey
      val customerParams: util.Map[String, Object] = new util.HashMap[String, Object]()
      customerParams.put("source", token)
      customerParams.put("plan", plan)
      customerParams.put("email", createdBy)
      val customer = Customer.create(customerParams)
      customer
    }
  }

  def cancelSubscription(customerId: String, subscriptionId: String): Try[Subscription] = {
    Try {
      Stripe.apiKey = ApplicationConfig.stripeSecretKey
      val cu = Customer.retrieve(customerId)
      val subscription = cu.getSubscriptions().retrieve(subscriptionId)
      val canceledSubscription = subscription.cancel(null)
      canceledSubscription
    }
  }
}
