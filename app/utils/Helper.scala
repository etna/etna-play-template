package utils

import java.security.MessageDigest
import play.api.i18n.Messages
import java.util.Date
import java.text.SimpleDateFormat
import java.util
import play.api.Logger

/**
 * Created by stipe on 03.03.14..
 */
object Helper {
  private val dateFormatter = new SimpleDateFormat("dd.MM.yyyy")

  def md5(s: String): Option[String] = {
    if(s.isEmpty)
      None
    else
      Some(MessageDigest.getInstance("MD5").digest(s.getBytes).map("%02x".format(_)).mkString)
  }

  def base64Encode(in: String): String ={
    if(in.nonEmpty)
      new sun.misc.BASE64Encoder().encode(in.getBytes())
    else ""
  }

  def base64Decode(in: String): String = {
    new String(new sun.misc.BASE64Decoder().decodeBuffer(in))
  }

  def formatDate(d: Date): String = {
    dateFormatter.format(d)
  }

  def formatDateFromSeconds(l: Long): String = {
    val d = new Date(l * 1000)
    formatDate(d)
  }

  def currentTimeInSeconds = System.currentTimeMillis() / 1000

  def time[A](name: String)(block: => A) = {
    val s = System.nanoTime
    val ret = block
    Logger.debug(s"TIMER $name - took "+(System.nanoTime-s)/1e6+"ms")
    ret
  }

  def getExtension(fileName:String):String= {
    val dotIndex = fileName.lastIndexOf(".")
    if(dotIndex>0){
      fileName.substring(dotIndex+1)
    }else{
      ""
    }
  }

}
