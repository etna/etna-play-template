package utils

import org.joda.time.DateTime
import play.api.templates.Html
import play.api.i18n.{Lang, Messages}

/**
 * Created by stipe on 03.03.14..
 */
object TemplateHelper{

  def copyright = {
    s"Ⓒ $year $company ${message("copyright")}"
  }

  def application = {
    message("application")
  }

  def company = {
    message("company")
  }

  def year = {
    new DateTime().year().get()
  }

  def message(key: String) = {
    Messages(key)(ApplicationConfig.defaultLang)
  }

  def info(message: String) = {
    play.twirl.api.Html.apply(
      s"""<script type="text/javascript">
          $$(document).ready(function() {
            etna.notifications.info("$message")
          });
      </script>"""
    )
  }

  def error(message: String) = {
    play.twirl.api.Html.apply(
      s"""<script type="text/javascript">
          $$(document).ready(function() {
            etna.notifications.error("$message")
          });
      </script>"""
    )
  }

  def warning(message: String) = {
    play.twirl.api.Html.apply(
      s"""<script type="text/javascript">
          $$(document).ready(function() {
            etna.notifications.warning("$message")
          });
      </script>"""
    )
  }

  def success(message: String) = {
    play.twirl.api.Html.apply(
      s"""<script type="text/javascript">
          $$(document).ready(function() {
            etna.notifications.success("$message")
          });
      </script>"""
    )
  }

}
