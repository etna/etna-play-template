package utils

import java.io.{ByteArrayInputStream, FileInputStream, File}
import java.nio.file.Files

import com.amazonaws.AmazonServiceException
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model._
import play.api.Logger

/**
 * Amazon Manager: S3
 */
object AmazonManager {
  import ApplicationConfig._
  val credentials = new BasicAWSCredentials(amazonAccessKey, amazonSecretKey)

  val s3 = new AmazonS3Client(credentials)

  def initBucket(bucket:String) = {
    try {
      s3.createBucket(bucket)
    } catch{
      case ex:AmazonServiceException =>
        //We ignore exception: Service: Amazon S3; Status Code: 409; Error Code: BucketAlreadyOwnedByYou
        if(ex.getStatusCode!=409){
          Logger.error(ex.getMessage)
        }
      case ex:Exception =>
        Logger.error(ex.getMessage)
    }
  }

  def uploadFile(key:String, file:File, bucket:String=amazonS3Bucket):PutObjectResult = Helper.time(s"AmazonManager.uploadFile($key)"){
    s3.putObject(bucket, key, file)
  }

  def uploadString(key:String, payload:String, bucket:String=amazonS3Bucket):PutObjectResult = Helper.time(s"AmazonManager.uploadString($key)"){
    val bytes = payload.getBytes
    val objectMetadata = new ObjectMetadata()
    objectMetadata.setContentLength(bytes.length)

    s3.putObject(bucket, key, new ByteArrayInputStream(bytes), objectMetadata)
  }

  def downloadFile(key:String, bucket:String=amazonS3Bucket):Option[File] = Helper.time(s"AmazonManager.downloadFile($key)"){
    try{
      val file = Files.createTempFile("s3-", Helper.getExtension(key)).toFile
      file.deleteOnExit()
      val meta = s3.getObject(new GetObjectRequest(bucket, key), file)
      Some(file)
    }catch{
      case ex:Exception =>
        //Logger.error(s"There was a problem while downloading file $key from AWS S3. Reason: ${ex.getMessage}",ex)
        None
    }
  }

  def checkFile(key:String, bucket:String=amazonS3Bucket):Boolean = Helper.time(s"AmazonManager.checkFile($key)"){
    try{
      s3.getObjectMetadata(bucket, key)
      true
    }catch{
      case ex:Exception =>
        false
    }
  }

  def deleteFile(key:String, bucket:String=amazonS3Bucket) = Helper.time(s"AmazonManager.deleteFile($key)"){
    s3.deleteObject(bucket, key)
  }

  def listBucketObjects(prefix: String, bucket: String = amazonS3Bucket): ObjectListing = Helper.time(s"AmazonManager.listBucketObjects") {
    s3.listObjects(bucket, prefix)
  }

}
