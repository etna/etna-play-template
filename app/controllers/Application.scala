package controllers

import java.util.UUID

//import akka.actor.Status.Success
import play.api.libs.json.Json
import play.api.mvc._
import utils.{Helper, ApplicationConfig}
import views.html
import play.api.data.Form
import play.api.data.Forms._
import models.User

import scala.util.{Success, Failure}

object Application extends AppController {

  def index = IsAuthenticated { user => implicit request =>
    Ok(views.html.index(user))
  }

  /**
   * Authentication related actions
   */
  val loginForm = Form(
    tuple(
      "username" -> text,
      "password" -> text,
      "redirect" -> text
    ) verifying (message("login.error"), result => result match {
      case (username, password, redirect) => User.authenticate(username, password).isDefined
    })
  )

  def login(redirect: String) = Action { implicit request =>
    Ok(html.login(loginForm.bind(Map("redirect" -> redirect))))
  }

  def logout = IsAuthenticated { user => implicit request =>
    Redirect(routes.Application.login("/")).withNewSession
  }

  def authenticate = Action { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(html.login(formWithErrors))
      },
      user => {
        Redirect(user._3).withSession("username" -> user._1)
      }
    )
  }

  def register = Action { implicit request =>
    Ok(html.register())
  }

  def registerNewUser = Action(parse.json) { implicit request =>

    val email = (request.body  \ "email").as[String]
    val uuid = UUID.randomUUID().toString

    User.register(email, uuid) match {
      case Success(msg) =>
        // create activation link
        val url = s"${ApplicationConfig.host}/activation/${uuid}"
        // send mail
        // TODO
        // send response
        Results.Ok(Json.obj(
          "head" -> Json.obj(
            "message" -> "Email message containing a link for account activation will be delivered to the address you just entered.",
            "success" -> true))).withHeaders(play.api.http.HeaderNames.CACHE_CONTROL -> "no-cache")
      case Failure(ex) =>
        Results.InternalServerError(Json.obj(
          "head" -> Json.obj(
            "message" -> s"User cannot be registred! Reason: ${ex.getMessage}",
            "success" -> false)))
    }

  }

  def registrationComplete = Action {
    Ok(html.registrationComplete())
  }

  def activation(uuid: String) = Action { request =>
    val user = User.findByUuid(uuid)
    Ok(html.activation(user.getOrElse(User.DUMMY_USER)))
  }

  def activate = Action(parse.json) { implicit request =>

    val email = (request.body  \ "email").as[String]
    val firstName = (request.body  \ "firstName").as[String]
    val lastName = (request.body  \ "lastName").as[String]
    val password = (request.body  \ "password").as[String]

    val updateData = Map(
      "firstName" -> firstName,
      "lastName" -> lastName,
      "password" -> Helper.md5(password).get,
      "active" -> true)

    User.update(email, updateData, email) match {
      case Success(msg) =>
        // send response
        Results.Ok(Json.obj(
          "head" -> Json.obj(
            "message" -> s"User ${email} activated.",
            "success" -> true))).withSession("username" -> email)
      case Failure(ex) =>
        Results.InternalServerError(Json.obj(
          "head" -> Json.obj(
            "message" -> s"User cannot be activated! Reason: ${ex.getMessage}",
            "success" -> false)))
    }
  }

}