package controllers

import play.api.mvc._
import models.User
import scala.concurrent.Future
import play.api.libs.iteratee.{Enumerator, Iteratee}
import play.api.libs.json.{Json, JsValue}
import scala.Some
import play.api.mvc.SimpleResult
import play.api.libs.concurrent.Execution.Implicits._

/**
 * Created by stipe on 03.03.14..
 */
trait Secured {

  /**
   * Retrieve the connected user email.
   */
  private def username(request: RequestHeader) = request.session.get("username")

  /**
   * Redirect to login if the user in not authorized.
   */
  private def onUnauthorized(request: RequestHeader) = Results.Redirect(routes.Application.login(request.path))

  // --


  /*
  def IsAuthenticated(f: => String => Request[AnyContent] => Result) = Security.Authenticated(username, onUnauthorized) { user =>
    Action(request => f(user)(request))
  }
  */

  /**
   * Action for authenticated users.
   */
  def IsAuthenticated(f: => User => Request[AnyContent] => Result) = Security.Authenticated(username, onUnauthorized) { username =>
    val _user = User.findByUsername(username)
    if(_user.nonEmpty)
      Action(request => f(_user.get)(request))
    else
      Action(request => Results.Forbidden(views.html.defaultpages.unauthorized.render()))
  }

  def IsAuthenticated[A](bodyParser: BodyParser[A])(f: => User => Request[A] => Result) = Security.Authenticated(username, onUnauthorized) { user =>
    val _user = User.findByUsername(user)
    if(_user.nonEmpty)
      Action(bodyParser)(request => f(_user.get)(request))
    else
      Action(bodyParser)(request => Results.Forbidden(views.html.defaultpages.unauthorized.render()))
  }

  /**
   * Check if the connected user is authorized for this action
   */
  def IsAuthorized(role: String)(f: => User => Request[AnyContent] => Result) = IsAuthenticated { user => request =>
    if(user.hasRole(role)) {
      f(user)(request)
    } else {
      Results.Forbidden(views.html.defaultpages.unauthorized.render())
    }
  }

  def IsAuthorized[A](bodyParser: BodyParser[A])(role: String)(f: => User => Request[A] => Result) = IsAuthenticated(bodyParser) { user => request =>
    if(user.hasRole(role)) {
      f(user)(request)
    } else {
      Results.Forbidden(views.html.defaultpages.unauthorized.render())
    }
  }

  /**
   * Async action for authenticated users.
   */
  def IsAuthenticatedAsync(f: => User => Request[AnyContent] => Future[Result]) = Security.Authenticated(username, onUnauthorized) { user =>
    Action.async{ implicit request =>
      val _user = User.findByUsername(user)
      if(_user.nonEmpty)
        f(_user.get)(request)
      else
        Future.successful(Results.Forbidden(views.html.defaultpages.unauthorized.render()))
    }
  }

  /**
   * Check if the connected user is authorized for this async action
   */
  def IsAuthorizedAsync(role: String)(f: => User => Request[AnyContent] => Future[Result]) = IsAuthenticatedAsync { user => request =>
    if(user.hasRole(role)) {
      f(user)(request)
    } else {
      Future.successful(Results.Forbidden(views.html.defaultpages.unauthorized.render()))
    }
  }


}