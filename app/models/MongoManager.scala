package models

import com.mongodb.ServerAddress
import com.mongodb.casbah.{MongoClient, MongoCredential}
import utils.ApplicationConfig

/**
 * Created by stipe on 2.4.2014.
 */
class MongoManager(val datasource: String = ApplicationConfig.environment) {
  private val config = ApplicationConfig.config
  private val mongoServer = new ServerAddress(hostname, port)

  private val credential = MongoCredential.createMongoCRCredential(user, database, password.toCharArray)

  lazy val db = MongoClient(mongoServer, List(credential))(database)

  lazy val hostname = config.getString(s"mongo.$datasource.host")
  lazy val port = config.getInt(s"mongo.$datasource.port")
  lazy val database = config.getString(s"mongo.$datasource.database")

  private lazy val user = config.getString(s"mongo.$datasource.user")
  private lazy val password = config.getString(s"mongo.$datasource.password")

}


object MongoManager {
  val instance = new MongoManager(ApplicationConfig.environment)
}
