package models

import com.mongodb.casbah.commons.MongoDBObject
import com.mongodb.casbah.Imports._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import java.util.Date
import play.api.{Logger, Play}

/**
 * Created by stipe on 3.4.2014..
 */
object History {
  val collection = MongoManager.instance.db("history")

  def log(collectionName: String, action: String, who: String, data: MongoDBObject) {
    Future {
      val doc = MongoDBObject(
        "collectionName" -> collectionName,
        "action" -> action,
        "when" -> new Date(),
        "who" -> who,
        "data" -> data
      )

      collection.insert(doc, WriteConcern.Safe)
    }
  }
}
