import play.api._

import play.api.mvc._
import play.api.mvc.RequestHeader
import scala.concurrent.Future
import utils.{TemplateHelper, Helper}

object Global extends GlobalSettings {

  override def onStart(app: Application) {
    Logger.info(s"Starting ${TemplateHelper.application} application")
  }

  override def onStop(app: Application) {
  }

  override def doFilter(action: EssentialAction) = LoggingFilter(action)
}

object LoggingFilter extends Filter {
  import play.api.libs.concurrent.Execution.Implicits._

  override def apply(next: (RequestHeader) => Future[Result])(rh: RequestHeader) = {
    val start = System.currentTimeMillis

    def logTime(result: Result): Result = {
      val time = System.currentTimeMillis - start
      Logger.debug(s"${rh.method} ${rh.uri} took ${time}ms and returned ${result.header.status}")
      result.withHeaders("Request-Time" -> time.toString)
    }

    val resultF = next(rh)
    resultF foreach logTime
    resultF
  }
}