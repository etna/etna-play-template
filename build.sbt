import play.twirl.sbt.Import._

name := "etna-play-template"

organization  := "hr.etna"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion  := "2.11.6"

resolvers ++= Seq(
  "jasperreports repo" at "http://jasperreports.sourceforge.net/maven2/"
)

scalacOptions ++= Seq(
  //Emit warning for usages of deprecated APIs
  "-deprecation",
  //Emit warning for usages of features that should be imported explicitly
  "-feature",
  //Enable additional warnings where generated code depends on assumptions
  "-unchecked",
  //Warn when dead code is identified
  "-Ywarn-dead-code"
)

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  "org.webjars"               %%  "webjars-play"                % "2.3.0-2",
  "org.webjars"               %   "bootstrap"                   % "3.3.4",
  "org.webjars"               %   "angularjs"                   % "1.3.15",
  "org.webjars"               %   "jquery"                      % "2.1.4",
  "org.webjars"               %   "font-awesome"                % "4.3.0-2",
  "joda-time"                 %   "joda-time"                   % "2.7",
  "org.mongodb"               %%  "casbah"                      % "2.8.1",
  "com.amazonaws"             %   "aws-java-sdk"                % "1.9.33",
  "com.stripe"                %   "stripe-java"                 % "1.27.1",
  "org.julienrf"              %%  "play-jsmessages"             % "1.6.2",
  "com.github.dzsessona"      %%  "scamandrill"                 % "1.1.0"
)

TwirlKeys.templateImports += "utils.TemplateHelper._"
